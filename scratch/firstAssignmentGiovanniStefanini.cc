/*
 * Primo assignment.
 * 
 * Creare una simulazione di una rete che abbia le seguenti caratteristiche:
 * 
 *    Almeno 4 nodi
 *    Il nodo 0 comunichi col nodo 1
 *    Il nodo 2 comunichi col nodo 3
 * 
 * Fare in modo che sia possibile collezionare un qualsiasi dato relativo alla comunicazione tra i nodi. E.g.:
 * 
 *    RTT
 *    Throughput
 *    Packet loss
 *    Parametri del TCP (se viene usato il TCP)
 *    Altro
 * 
 * Si fornisca:
 * 
 *    Il codice, meglio se tramite un link ad una repository GitLab (in questo caso indicare il nome dello script e il suo path).
 *    UNA pagina di report con i dati ottenuti, meglio se visualizzati con un grafico (o una tabella).
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// Network Topology
//
//           10.1.1.0
// (C) n0 -------------- (S) n1
//        point-to-point
//
//
//           12.1.2.0                  14.1.3.0
// (C) n2 -------------- (S) n3 (C) -------------- (S) n4 
//        point-to-point            point-to-point
//
//
//                        16.1.4.0
//              (C) n2 -------------- (S) n4   n5   n6    n7
//                     point-to-point      |    |    |    |
//                                         ================
//                                           LAN 18.1.5.0
//
// NB. nodo 2 è client per due nodi (per nodo 3 e per nodo 4)
// NB. nodo 3 è sia server (per nodo 2) che client (per nodo 4)
// NB. nodo 4 è server per due nodi (nodo 3 e nodo 2)

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstAssignment"); //ci dice che è bene non avere due classi nello stesso file, ne puoi scrivere solo uno

int
main(int argc, char* argv[])
{
    bool verbose = true;
    uint32_t nCsma = 3;

    CommandLine cmd(__FILE__); //classe che serve per gestire i parametri della command line (devo scrivere parser). 
    cmd.AddValue("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);

    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    nCsma = nCsma == 0 ? 1 : nCsma;

    //creo 5 nodi p2p
    NodeContainer nodes1;
    nodes1.Create(2); // nodi 0 e 1 

    NodeContainer nodes2;
    nodes2.Create(2); // nodi 2 e 3

    NodeContainer nodes3; 
    nodes3.Add(nodes2.Get(1)); // nodo 3 che viene aggiunto per realizzare rete p2p
    nodes3.Create(1); // nodo 4 

    NodeContainer nodes4; 
    nodes4.Add(nodes2.Get(0)); // nodo 2 che viene aggiunto per realizzare rete p2p
    nodes4.Add(nodes3.Get(1)); // nodo 4 che viene aggiunto per realizzare rete p2p 

    //creo 4 nodi csma
    NodeContainer csmaNodes;
    csmaNodes.Add(nodes3.Get(1)); // nodo 4 viene aggiunto per ralizzare rete LAN
    csmaNodes.Create(nCsma); // nodo 5, 6 e 7

    //definisco le caratteristiche del canale PointToPoint
    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("1Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("10ms"));

    //installo 4 connessioni pointToPoint
    NetDeviceContainer p2pDevices1;
    p2pDevices1 = pointToPoint.Install(nodes1);
    NetDeviceContainer p2pDevices2;
    p2pDevices2 = pointToPoint.Install(nodes2);
    NetDeviceContainer p2pDevices3;
    p2pDevices3 = pointToPoint.Install(nodes3);
    NetDeviceContainer p2pDevices4;
    p2pDevices4 = pointToPoint.Install(nodes4);

    //definisco le caratteristiche del canale CSMA
    CsmaHelper csma; // -------
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));

    //installo 1 connesione csma
    NetDeviceContainer csmaDevices; //----
    csmaDevices = csma.Install(csmaNodes);

    //Installo nei nodi lo stack dei protocolli
    InternetStackHelper stack;
    stack.Install(nodes1);
    stack.Install(nodes2);
    //stack.Install(nodes3.Get(1));
    stack.Install(csmaNodes); // ----

    //Assegno ai nodi del PontiToPoint l'indirizzo IPv4
    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces1; 
    p2pInterfaces1 = address.Assign(p2pDevices1); //assegna l'interfaccia p2p 10.1.1.0 al nodo 0 e 1

    address.SetBase("12.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces2; 
    p2pInterfaces2 = address.Assign(p2pDevices2); //assegna l'interfaccia p2p 12.1.2.0 al nodo 2 e 3

    address.SetBase("14.1.3.0", "255.255.255.0"); 
    Ipv4InterfaceContainer p2pInterfaces3; 
    p2pInterfaces3 = address.Assign(p2pDevices3); //assegna l'interfaccia p2p 14.1.3.0 ai nodi 3 e 4

    address.SetBase("16.1.4.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces4; 
    p2pInterfaces4 = address.Assign(p2pDevices4); // assegna l'interfaccia p2p 16.1.4.0 ai nodi 2 e 4

    //Assegno ai nodi del csma l'indirizzo IPv4
    address.SetBase("18.1.5.0", "255.255.255.0"); // assegna l'interfaccia csma 18.1.5.0 ai nodi 4,5,6 e 7
    Ipv4InterfaceContainer csmaInterfaces;
    csmaInterfaces = address.Assign(csmaDevices);

    //---primi due nodi---
    // nodo0  client - nodo1 server
    UdpEchoServerHelper echoServer1(9); //Echo server: nodo in ascolto su una porta (9). Quando riceve un pacchetto risponde sullo stesso canale.

    ApplicationContainer serverApps1 = echoServer1.Install(nodes1.Get(1));
    serverApps1.Start(Seconds(1.0));
    serverApps1.Stop(Seconds(15.0));

    UdpEchoClientHelper echoClient1(p2pInterfaces1.GetAddress(1), 9);
    echoClient1.SetAttribute("MaxPackets", UintegerValue(9)); //numero di pacchetti che deve spedire il Client
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(1.0))); //intervallo di tempo tra un pacchetto e l'altro
    echoClient1.SetAttribute("PacketSize", UintegerValue(2048));

    ApplicationContainer clientApps1 = echoClient1.Install(nodes1.Get(0));
    clientApps1.Start(Seconds(2.0));
    clientApps1.Stop(Seconds(15.0));

    //---secondi due nodi---
    // nodo2  client - nodo3 server
    UdpEchoServerHelper echoServer2(5);

    ApplicationContainer serverApps2 = echoServer2.Install(nodes2.Get(1));
    serverApps2.Start(Seconds(0.5));
    serverApps2.Stop(Seconds(30.0));

    UdpEchoClientHelper echoClient2(p2pInterfaces2.GetAddress(1), 5); //client con tanti pacchetti ma veloce
    echoClient2.SetAttribute("MaxPackets", UintegerValue(15));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(0.5)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps2 = echoClient2.Install(nodes2.Get(0));
    clientApps2.Start(Seconds(1.5));
    clientApps2.Stop(Seconds(15.0));

    //---terzi due nodi---
    // nodo3  client - nodo4 server
    UdpEchoServerHelper echoServer3(3);

    ApplicationContainer serverApps3 = echoServer3.Install(nodes3.Get(1));
    serverApps3.Start(Seconds(2.0));
    serverApps3.Stop(Seconds(15.0));

    UdpEchoClientHelper echoClient3(p2pInterfaces3.GetAddress(1), 3); //client con pochi pacchetti
    echoClient3.SetAttribute("MaxPackets", UintegerValue(4)); // pochi pacchetti lenti
    echoClient3.SetAttribute("Interval", TimeValue(Seconds(3.0)));
    echoClient3.SetAttribute("PacketSize", UintegerValue(4096));

    ApplicationContainer clientApps3 = echoClient3.Install(nodes3.Get(0));
    clientApps3.Start(Seconds(2.5));
    clientApps3.Stop(Seconds(15.0));

    //---quarti due nodi (pointtopoint) e quattro nodi (LAN)---
    // nodo2  client - nodo4 server & nodo4 nodo5 nodo6 nodo7
    UdpEchoServerHelper echoServer4(99);

    ApplicationContainer serverApps4 = echoServer4.Install(csmaNodes.Get(nCsma));
    serverApps4.Start(Seconds(1.0));
    serverApps4.Stop(Seconds(15.0));

    UdpEchoClientHelper echoClient4(csmaInterfaces.GetAddress(nCsma), 99);
    echoClient4.SetAttribute("MaxPackets", UintegerValue(10));
    echoClient4.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient4.SetAttribute("PacketSize", UintegerValue(3048));

    ApplicationContainer clientApps = echoClient4.Install(nodes2.Get(0));
    clientApps.Start(Seconds(2.0));
    clientApps.Stop(Seconds(15.0));

    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    //per tracciare i pacchetti con Pcap p2p
    //pointToPoint.EnablePcapAll("firstAssGioSteP2P");//cattura tutti i pacchetti che passano dalla rete e li chiama sicthAll. 
    pointToPoint.EnablePcap("firstAssGioSteP2P", p2pDevices4.Get(0), true);//cattura tutti i pacchetti del nodo 2 come client del nodo 4
    pointToPoint.EnablePcap("firstAssGioSteP2P", p2pDevices4.Get(1), true);//cattura tutti i pacchetti del nodo 4 come server del nodo 2

    //per tracciare i pacchetti con Pcap csma
    //csma.EnablePcapAll("firstAssGioSteCSMA");
    csma.EnablePcap("firstAssGioSteCSMA", csmaDevices.Get(0), true); //cattura i pacchetti del nodo 4 nel CSMA
    csma.EnablePcap("firstAssGioSteCSMA", csmaDevices.Get(3), true); //cattura i pacchetti del nodo 7 nel CSMA

    Simulator::Stop(Seconds(15.0));
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
