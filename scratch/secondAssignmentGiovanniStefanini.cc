/*
    Second Assignment:

    Costruire una rete formata da almeno 4 nodi wireless: 2 Access Points  (AP) e 2 nodi (STA). Se necessario, aumentare il numero di nodi STA.

    I due AP devono essere "vicini" (spazialmente) in modo da subire mutua interferenza. 
    Impostare i 2 AP in modo che usino 2 canali WiFi tali da generare mutua interferenza. 

        Si associno i nodi STA a uno dei due AP, in modo che parte siano associati a un AP, e parte all'altro.
        [opzionale] Si connettano i due AP tramite una rete CSMA o P2P. In questo caso si provveda a mettere le necessarie regole di routing.
        Si generi traffico da o per i nodi STA (a piacere).

    La simulazione deve essere in grado di usare, alternativamente, i seguenti modelli per il canale:

        YansWiFiChannel
        SpectrumWiFiChannel (MultiModelSpectrumChannel)

    Si verifichi che:

        1. I risultati della simulazione siano coerenti tra i vari modelli di canale se si usa un solo AP.
        2. I risultati della simulazione siano differenti tra i modelli Yans e Spectrum se si usano due AP.
        3. La velocità di esecuzione della simulazione nei vari casi
*/

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ssid.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/single-model-spectrum-channel.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/non-communicating-net-device.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/wifi-net-device.h"
#include <chrono>
#include <ctime> 

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("SecondAssignment"); 

Ptr<SpectrumModel> SpectrumModelWifi5180MHz; // Spectrum model at 5180 MHz.

// Initializer for a static spectrum model centered around 5180 MHz 
class static_SpectrumModelWifi5180MHz_initializer
{
  public:
    static_SpectrumModelWifi5180MHz_initializer()
    {
        BandInfo bandInfo;
        bandInfo.fc = 5180e6;
        bandInfo.fl = 5180e6 - 10e6;
        bandInfo.fh = 5180e6 + 10e6;

        Bands bands;
        bands.push_back(bandInfo);

        SpectrumModelWifi5180MHz = Create<SpectrumModel>(bands);
    }
};

// Static instance to initizlize the spectrum model around 5180 MHz.
static_SpectrumModelWifi5180MHz_initializer static_SpectrumModelWifi5180MHz_initializer_instance;

int
main(int argc, char* argv[])
{
    auto start = std::chrono::system_clock::now(); //assegna a start il tempo prima dell’esecuzione
    bool verbose = true;
    uint32_t nWifi = 3; //3 nodi STA per ogni rete Wifi
    std::string wifiType = "ns3::SpectrumWifiPhy";//"ns3::YansWifiPhy"; //"ns3::SpectrumWifiPhy"; //serve per cambiare il tipo di canale
    std::string errorModelType = "ns3::NistErrorRateModel";
    uint16_t frequency = 5180;
    bool udp = false; //true = UDP --- false = TCP //serve per cambiare tra UDP e TCP
    bool multi = true; //true = MultiModelSpectrum --- false = SingleModelSpectrum //serve per cambiare tra SingleModel e MultiModel
    bool enablePcap = true; // true = Abilita i Pcap --- false = Pcap non catturati
    double simulationTime = 10;  // seconds

    CommandLine cmd(__FILE__);
    cmd.AddValue("nWifi", "Number of wifi STA devices", nWifi);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.AddValue("wifiType", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", udp);
    cmd.AddValue("multi", "Multi Model if set to 1, Single Model otherwise", multi);
    cmd.AddValue("enablePcap", "enable pcap output", enablePcap);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);

    cmd.Parse(argc, argv);

    std::cout << "wifiType: " << wifiType << std::endl;
    if(udp){
        std::cout << "UDP" << std::endl;
    }
    else{
        std::cout << "TCP" << std::endl;
    }

    // The underlying restriction of 18 is due to the grid position allocator's configuration; the grid layout will exceed the bounding box if more than 18 nodes are provided.
    if (nWifi > 18)
    {
        std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box"
                  << std::endl;
        return 1;
    }

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    uint32_t payloadSize;
    if (udp)
    {
        payloadSize = 972; // 1000 bytes IPv4
    }
    else
    {
        payloadSize = 1448; // 1500 bytes IPv6
        Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
    }

        
    NodeContainer wifiStaNodes, wifiStaNodes2;
    wifiStaNodes.Create(nWifi); //nodo 0, 1 e 2
    wifiStaNodes2.Create(nWifi); //nodo 3, 4 e 5

    NodeContainer wifiApNode;
    wifiApNode.Create(2); //creo due nodi Access Point //nodo 6 e 7

    //Yans
    YansWifiPhyHelper yansPhy;
    YansWifiChannelHelper channel; //creo un canale YansWifiChannel 
    channel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                "Frequency",
                                DoubleValue(frequency * 1e6));
    channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    yansPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    yansPhy.SetChannel(channel.Create());

    //Multi Model Spectrum
    SpectrumWifiPhyHelper spectrumMMPhy;
    Ptr<MultiModelSpectrumChannel> mMspectrumChannel;
    mMspectrumChannel = CreateObject<MultiModelSpectrumChannel>();
    Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
    lossModel->SetFrequency(frequency * 1e6);
    mMspectrumChannel->AddPropagationLossModel(lossModel);
    Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();
    mMspectrumChannel->SetPropagationDelayModel(delayModel);
    spectrumMMPhy.SetChannel(mMspectrumChannel);
    spectrumMMPhy.SetErrorRateModel(errorModelType);
    spectrumMMPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);

    //WifiMacHelper object is used to set MAC parameters.    
    WifiMacHelper mac; //MAC prima rete e seconda rete

    //WifiHelper
    WifiHelper wifi; 
    wifi.SetStandard(WIFI_STANDARD_80211n); //imposta lo standard 802.11n
    wifi.SetRemoteStationManager("ns3::IdealWifiManager");

    NetDeviceContainer staDevices, staDevices2;
    NetDeviceContainer apDevices, apDevices2;
    Ssid ssid;

    //configurazione dei dispositivi della rete
    if (wifiType == "ns3::YansWifiPhy") {
        //Reti con YANS
        //Rete 1
        ssid = Ssid("network-1"); //Ssid prima rete
        yansPhy.Set("ChannelSettings", StringValue(std::string("{36, 0, BAND_5GHZ, 0}")));

        //STA devices 1 
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid)); // setto il MAC per STA
        staDevices = wifi.Install(yansPhy, mac, wifiStaNodes);

        //AP devices 1 
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); // setto il MAC per AP
        apDevices = wifi.Install(yansPhy, mac, wifiApNode.Get(0));

        //Rete 2
        ssid = Ssid("network-2"); //Ssid seconda rete
        yansPhy.Set("ChannelSettings", StringValue(std::string("{38, 0, BAND_5GHZ, 0}")));

        //STA devices 2 
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid)); // setto il MAC per STA
        staDevices2 = wifi.Install(yansPhy, mac, wifiStaNodes2);

        //AP devices 2
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); // setto il MAC per AP
        apDevices2 = wifi.Install(yansPhy, mac, wifiApNode.Get(1));  
    }
    else if (wifiType == "ns3::SpectrumWifiPhy") {
        if(multi) {
            //Reti con MULTIMODEL
            //Rete 1
            ssid = Ssid("network-1"); //Ssid prima rete
            spectrumMMPhy.Set("ChannelSettings", StringValue(std::string("{36, 0, BAND_5GHZ, 0}")));

            //STA devices 1 
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            staDevices = wifi.Install(spectrumMMPhy, mac, wifiStaNodes);

            //AP devices 1
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); 
            apDevices = wifi.Install(spectrumMMPhy, mac, wifiApNode.Get(0));

            //Rete 2
            ssid = Ssid("network-2"); //Ssid seconda rete
            spectrumMMPhy.Set("ChannelSettings", StringValue(std::string("{38, 0, BAND_5GHZ, 0}")));

            //STA devices 2 
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            staDevices2 = wifi.Install(spectrumMMPhy, mac, wifiStaNodes2);

            //AP devices 2
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); 
            apDevices2 = wifi.Install(spectrumMMPhy, mac, wifiApNode.Get(1));
        }
        else {
            //--- ToDo Complete ---//
            //Reti SINGLEMODEL
            // non è stato implementato poichè non sono riuscito a risolvere il bug evidenziato a lezione
        }
    }
    else
    {
        NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
    }

    //Mobility
    MobilityHelper mobility;

    //Dispongo i nodi in una griglia 10x10: prima riga (3 STA) distanti 5m una dall'altra, seconda riga (2 AP) entrambi nel punto centrale, terza riga (3 STA) distanti 5m una dall'altra
    //metto i primi 3 STA sulla prima riga della griglia
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX",
                                DoubleValue(0.0),
                                "MinY",
                                DoubleValue(0.0),
                                "DeltaX",
                                DoubleValue(5.0),
                                "DeltaY",
                                DoubleValue(5.0),
                                "GridWidth",
                                UintegerValue(3),
                                "LayoutType",
                                StringValue("RowFirst")); //We have arranged our nodes on an initial grid

    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel"); //fa in modo che i nodi siano fissi
        
    mobility.Install(wifiStaNodes); //install the mobility models on the STA nodes.

    //metto gli access point al centro della griglia sulla seconda riga                           
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX",
                                DoubleValue(5.0),
                                "MinY",
                                DoubleValue(5.0),
                                "DeltaX",
                                DoubleValue(0.0),
                                "DeltaY",
                                DoubleValue(0.0),
                                "GridWidth",
                                UintegerValue(3),
                                "LayoutType",
                                StringValue("RowFirst")); //We have arranged our nodes on an initial grid

    mobility.Install(wifiApNode); // Almeno sta nel mezzo

    //metto gli altri 3 STA sulla terza riga della griglia
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX",
                                DoubleValue(0.0),
                                "MinY",
                                DoubleValue(10.0),
                                "DeltaX",
                                DoubleValue(5.0),
                                "DeltaY",
                                DoubleValue(5.0),
                                "GridWidth",
                                UintegerValue(3),
                                "LayoutType",
                                StringValue("RowFirst")); //We have arranged our nodes on an initial grid

    mobility.Install(wifiStaNodes2); //install the mobility models on the STA nodes.


    //Internet stack
    InternetStackHelper stack;
    Ipv4StaticRoutingHelper staticRoutingHelper; // serve per il routing statico dei nodi STA
    stack.SetRoutingHelper (staticRoutingHelper);
    stack.Install(wifiStaNodes);
    stack.Install(wifiStaNodes2);  
    stack.Install(wifiApNode);

    //Assegnazione degli indirizzi IP
    Ipv4AddressHelper address;

    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiAPInterfaces, wifiSTAInterfaces;
    wifiAPInterfaces = address.Assign(apDevices); //interfaccia wifiIp 1° nodo AP 
    wifiSTAInterfaces = address.Assign(staDevices); //interfaccia wifiIp nodi 3 Nodi STA

    NodeContainer::Iterator iter;
    for (iter = wifiStaNodes.Begin (); iter != wifiStaNodes.End (); iter++){
        Ptr<Ipv4StaticRouting> staticRouting;
        staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> ((*iter)->GetObject<Ipv4> ()->GetRoutingProtocol ());
        staticRouting->SetDefaultRoute ("<address of the AP>", 1 );
    }

    address.SetBase("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiAPInterfaces2, wifiSTAInterfaces2;
    wifiAPInterfaces2 = address.Assign(apDevices2); //interfaccia wifiIp 2° nodo AP 
    wifiSTAInterfaces2 = address.Assign(staDevices2); //interfaccia wifiIp2 nodi 3 Nodi STA

    NodeContainer::Iterator iter2;
    for (iter2 = wifiStaNodes2.Begin (); iter2 != wifiStaNodes2.End (); iter2++){
        Ptr<Ipv4StaticRouting> staticRouting2;
        staticRouting2 = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> ((*iter2)->GetObject<Ipv4> ()->GetRoutingProtocol ());
        staticRouting2->SetDefaultRoute ("<address of the AP>", 2 );
    }

    // Configurazione Application
    ApplicationContainer serverApps, serverApps2, serverAppsAP, serverAppsAP2;
    if(udp){
        uint16_t port, port2, portAP, portAP2;
        port = 9;
        port2 = 70;
        portAP = 49;
        portAP2 = 98;

        //Prima Rete Wireless
        //Server 1.1 sta sul nodo1
        UdpServerHelper server(port);
        serverApps = server.Install(wifiStaNodes.Get(1));
        serverApps.Start(Seconds(1.0));
        serverApps.Stop(Seconds(simulationTime + 1));

        //Server 1.2 sta sul nodo6
        UdpServerHelper serverAP(portAP);
        serverAppsAP = serverAP.Install(wifiApNode.Get(0));
        serverAppsAP.Start(Seconds(1.0));
        serverAppsAP.Stop(Seconds(simulationTime + 1));

        //Client 1.1 sta sul nodo0
        UdpClientHelper client1(wifiSTAInterfaces.GetAddress(1), port);
        client1.SetAttribute("MaxPackets", UintegerValue(20000));
        client1.SetAttribute("Interval", TimeValue(Seconds(0.0025))); //packet/s
        client1.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps1 = client1.Install(wifiStaNodes.Get(0));
        clientApps1.Start(Seconds(1.5));
        clientApps1.Stop(Seconds(simulationTime + 1));

        //Client 1.2 sta sul nodo2
        UdpClientHelper client2(wifiSTAInterfaces.GetAddress(1), port);
        client2.SetAttribute("MaxPackets", UintegerValue(15000));
        client2.SetAttribute("Interval", TimeValue(Seconds(0.005))); //packet/s
        client2.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps2 = client2.Install(wifiStaNodes.Get(2));
        clientApps2.Start(Seconds(1.5));
        clientApps2.Stop(Seconds(simulationTime + 1));

        //Client 1.3 sta sul nodo1
        UdpClientHelper client3(wifiAPInterfaces.GetAddress(0), portAP);
        client3.SetAttribute("MaxPackets", UintegerValue(15000));
        client3.SetAttribute("Interval", TimeValue(Seconds(0.005))); //packet/s
        client3.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps3 = client3.Install(wifiStaNodes.Get(1));
        clientApps3.Start(Seconds(1.5));
        clientApps3.Stop(Seconds(simulationTime + 1));

        //Seconda Rete Wireless
        //Server 2.1 sta sul nodo4
        UdpServerHelper server2(port2);
        serverApps2 = server2.Install(wifiStaNodes2.Get(1));
        serverApps2.Start(Seconds(2.0));
        serverApps2.Stop(Seconds(simulationTime + 1));

        //Server 2.2 sta sul nodo7
        UdpServerHelper serverAP2(portAP2);
        serverAppsAP2 = serverAP2.Install(wifiApNode.Get(1));
        serverAppsAP2.Start(Seconds(1.0));
        serverAppsAP2.Stop(Seconds(simulationTime + 1));

        //Client 2.1 sta sul nodo3
        UdpClientHelper client11(wifiSTAInterfaces2.GetAddress(1), port2);
        client11.SetAttribute("MaxPackets", UintegerValue(15000));
        client11.SetAttribute("Interval", TimeValue(Seconds(0.005))); //packet/s
        client11.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps11 = client11.Install(wifiStaNodes2.Get(0));
        clientApps11.Start(Seconds(2.5));
        clientApps11.Stop(Seconds(simulationTime + 1));

        //Client 2.2 sta sul nodo5
        UdpClientHelper client22(wifiSTAInterfaces2.GetAddress(1), port2);
        client22.SetAttribute("MaxPackets", UintegerValue(20000));
        client22.SetAttribute("Interval", TimeValue(Seconds(0.0025))); //packet/s
        client22.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps22 = client22.Install(wifiStaNodes2.Get(2));
        clientApps22.Start(Seconds(2.5));
        clientApps22.Stop(Seconds(simulationTime + 1));

        //Client 2.3 sta sul nodo4
        UdpClientHelper client33(wifiAPInterfaces2.GetAddress(0), portAP2);
        client33.SetAttribute("MaxPackets", UintegerValue(15000));
        client33.SetAttribute("Interval", TimeValue(Seconds(0.005))); //packet/s
        client33.SetAttribute("PacketSize", UintegerValue(1024)); //playload size
        ApplicationContainer clientApps33 = client33.Install(wifiStaNodes2.Get(1));
        clientApps33.Start(Seconds(1.5));
        clientApps33.Stop(Seconds(simulationTime + 1));
    }
    else {
        // TCP flow
        uint16_t port, port2, portAP, portAP2;
        port = 10;
        port2 = 80;
        portAP = 30;
        portAP2 = 110;
        
        //prima rete wireless
        //Server 1.1 sta sul nodo1
        Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
        PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
        serverApps = packetSinkHelper.Install(wifiStaNodes.Get(1));
        serverApps.Start(Seconds(1.0));
        serverApps.Stop(Seconds(simulationTime + 1));

        //Server 1.2 sta sul nodo6
        Address localAddressAP(InetSocketAddress(Ipv4Address::GetAny(), portAP));
        PacketSinkHelper packetSinkHelperAP("ns3::TcpSocketFactory", localAddressAP);
        serverAppsAP = packetSinkHelperAP.Install(wifiApNode.Get(0));
        serverAppsAP.Start(Seconds(1.0));
        serverAppsAP.Stop(Seconds(simulationTime + 1));

        //Client 1.1 sta sul nodo0
        OnOffHelper onoff1("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff1.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff1.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff1.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff1.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress1(InetSocketAddress(wifiSTAInterfaces.GetAddress(1), port));
        onoff1.SetAttribute("Remote", remoteAddress1);
        ApplicationContainer clientApp1 = onoff1.Install(wifiStaNodes.Get(0));
        clientApp1.Start(Seconds(1.5));
        clientApp1.Stop(Seconds(simulationTime + 1));

        //Client 1.2 sta sul nodo2
        OnOffHelper onoff2("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff2.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff2.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff2.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff2.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress2(InetSocketAddress(wifiSTAInterfaces.GetAddress(1), port));
        onoff2.SetAttribute("Remote", remoteAddress2);
        ApplicationContainer clientApp2 = onoff2.Install(wifiStaNodes.Get(2));
        clientApp2.Start(Seconds(1.5));
        clientApp2.Stop(Seconds(simulationTime + 1));

        //Client 1.3 sta sul nodo1
        OnOffHelper onoff3("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff3.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff3.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff3.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff3.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress3(InetSocketAddress(wifiAPInterfaces.GetAddress(0), portAP));
        onoff3.SetAttribute("Remote", remoteAddress3);
        ApplicationContainer clientApp3 = onoff3.Install(wifiStaNodes.Get(1));
        clientApp3.Start(Seconds(1.5));
        clientApp3.Stop(Seconds(simulationTime + 1));

        //seconda rete wireless
        //Server 2.1 sta sul nodo4
        Address localAddress2(InetSocketAddress(Ipv4Address::GetAny(), port2));
        PacketSinkHelper packetSinkHelper2("ns3::TcpSocketFactory", localAddress2);
        serverApps2 = packetSinkHelper2.Install(wifiStaNodes2.Get(1));
        serverApps2.Start(Seconds(1.0));
        serverApps2.Stop(Seconds(simulationTime + 1));

        //Server 2.2 sta sul nodo7
        Address localAddressAP2(InetSocketAddress(Ipv4Address::GetAny(), portAP2));
        PacketSinkHelper packetSinkHelperAP2("ns3::TcpSocketFactory", localAddressAP2);
        serverAppsAP2 = packetSinkHelperAP2.Install(wifiApNode.Get(1));
        serverAppsAP2.Start(Seconds(1.0));
        serverAppsAP2.Stop(Seconds(simulationTime + 1));

        //Client 2.1 sta sul nodo3
        OnOffHelper onoff11("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff11.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff11.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff11.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff11.SetAttribute("DataRate", DataRateValue(1000000000)); 
        AddressValue remoteAddress11(InetSocketAddress(wifiSTAInterfaces2.GetAddress(1), port2));
        onoff11.SetAttribute("Remote", remoteAddress11);
        ApplicationContainer clientApp11 = onoff11.Install(wifiStaNodes2.Get(0));
        clientApp11.Start(Seconds(1.5));
        clientApp11.Stop(Seconds(simulationTime + 1));

        //Client 2.2 sta sul nodo5
        OnOffHelper onoff22("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff22.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff22.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff22.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff22.SetAttribute("DataRate", DataRateValue(1000000000));
        AddressValue remoteAddress22(InetSocketAddress(wifiSTAInterfaces2.GetAddress(1), port2));
        onoff22.SetAttribute("Remote", remoteAddress22);
        ApplicationContainer clientApp22 = onoff22.Install(wifiStaNodes2.Get(2));
        clientApp22.Start(Seconds(1.5));
        clientApp22.Stop(Seconds(simulationTime + 1));

        //Client 2.3 sta sul nodo4
        OnOffHelper onoff33("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff33.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff33.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff33.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff33.SetAttribute("DataRate", DataRateValue(1000000000)); 
        AddressValue remoteAddress33(InetSocketAddress(wifiAPInterfaces2.GetAddress(0), portAP2));
        onoff33.SetAttribute("Remote", remoteAddress33);
        ApplicationContainer clientApp33 = onoff33.Install(wifiStaNodes2.Get(1));
        clientApp33.Start(Seconds(1.5));
        clientApp33.Stop(Seconds(simulationTime + 1));
    }

    //Abilita Internetwork Routing
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    //Creazione dei Pcap
    if (wifiType == "ns3::YansWifiPhy") {
        //per tracciare i pacchetti con Yans
        if (enablePcap){
            std::stringstream ss;
            if(udp) {
                ss << "wifi-Yans-UDP-";
            }
            else {
                ss << "wifi-Yans-TCP-";
            }
            yansPhy.EnablePcap(ss.str(), staDevices.Get(1)); //abilita il Pcap nodo1 della prima rete
            yansPhy.EnablePcap(ss.str(), staDevices2.Get(1)); //abilita il Pcap nodo4 della seconda rete 
            yansPhy.EnablePcap(ss.str(), apDevices); //abilita il Pcap dell'access Point della prima rete (nodo 6)
            yansPhy.EnablePcap(ss.str(), apDevices2); //abilita il Pcap dell'access Point della seconda rete (nodo 7)
        }
    }
    else if (wifiType == "ns3::SpectrumWifiPhy") {
        if(multi) {
            //per tracciare i pacchetti con MultiModel
            if (enablePcap){
                std::stringstream ss;
                if(udp) {
                    ss << "wifi-MultiModelSpectrum-UDP-";
                }
                else {
                    ss << "wifi-MultiModelSpectrum-TCP-";
                }
                spectrumMMPhy.EnablePcap(ss.str(), staDevices.Get(1)); //abilita il Pcap nodo1 della prima rete 
                spectrumMMPhy.EnablePcap(ss.str(), staDevices2.Get(1)); //abilita il Pcap nodo4 della seconda rete 
                spectrumMMPhy.EnablePcap(ss.str(), apDevices); //abilita il Pcap dell'access Point della prima rete (nodo 6)
                spectrumMMPhy.EnablePcap(ss.str(), apDevices2); //abilita il Pcap dell'access Point della seconda rete (nodo 7)
            }
        }
        else {
            //Canali SINGLEMODEL
            // non è stato implementato poichè non sono riuscito a risolvere il bug evidenziato a lezione
        }
    }

    // Flow monitor
    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    Simulator::Stop(Seconds(simulationTime + 1)); //serve per stoppare la simulation
    
    if(udp) {
        AnimationInterface anim("animSA.xml");
    }
    else {
        AnimationInterface anim("animSA.xml");
        anim.SetMaxPktsPerTraceFile(500000); //serve per limitare tutti i pacchetti nel caso venga usato TCP
    }
    //AnimationInterface anim("animSA.xml");

    Simulator::Run();

    flowMonitor->SerializeToXmlFile("flowMonitorSA.xml", true, true);

    flowMonitor->CheckForLostPackets (); 
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
    {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

        NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
            NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
            NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
            NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");
    }

    Simulator::Destroy();

    auto end = std::chrono::system_clock::now(); //assegna a end il tempo a fine esecuzione
    std::chrono::duration<double> elapsed_seconds = end-start; // tempo di esecuzione del codice
    std::cout << "Tempo di effettivo della simulazione: " << elapsed_seconds.count() << "s"
              << std::endl;

    return 0;
}
